Feature: Funds Allocation

  Scenario: Investment funds are allocated evenly
    Given the following investment funds:
      | id | type     | name                  |
      | 1  | POLISH   | Fundusz Polski 1      |
      | 2  | POLISH   | Fundusz Polski 2      |
      | 3  | FOREIGN  | Fundusz Zagraniczny 1 |
      | 4  | FOREIGN  | Fundusz Zagraniczny 2 |
      | 5  | FOREIGN  | Fundusz Zagraniczny 3 |
      | 6  | MONETARY | Fundusz Pieniężny 1   |
    When we allocate funds in amount of 10000 using SAFE strategy
    Then the allocation result is:
      | id | type     | name                  | amount | percentage |
      | 1  | POLISH   | Fundusz Polski 1      | 1000   | 10         |
      | 2  | POLISH   | Fundusz Polski 2      | 1000   | 10         |
      | 3  | FOREIGN  | Fundusz Zagraniczny 1 | 2500   | 25         |
      | 4  | FOREIGN  | Fundusz Zagraniczny 2 | 2500   | 25         |
      | 5  | FOREIGN  | Fundusz Zagraniczny 3 | 2500   | 25         |
      | 6  | MONETARY | Fundusz Pieniężny 1   | 500    | 5          |
    And not allocated amount is 0

  Scenario: Not the whole amount can be allocated
    Given the following investment funds:
      | id | type     | name                  |
      | 1  | POLISH   | Fundusz Polski 1      |
      | 2  | POLISH   | Fundusz Polski 2      |
      | 3  | FOREIGN  | Fundusz Zagraniczny 1 |
      | 4  | FOREIGN  | Fundusz Zagraniczny 2 |
      | 5  | FOREIGN  | Fundusz Zagraniczny 3 |
      | 6  | MONETARY | Fundusz Pieniężny 1   |
    When we allocate funds in amount of 10001 using SAFE strategy
    Then the allocation result is:
      | id | type     | name                  | amount | percentage |
      | 1  | POLISH   | Fundusz Polski 1      | 1000   | 10         |
      | 2  | POLISH   | Fundusz Polski 2      | 1000   | 10         |
      | 3  | FOREIGN  | Fundusz Zagraniczny 1 | 2500   | 25         |
      | 4  | FOREIGN  | Fundusz Zagraniczny 2 | 2500   | 25         |
      | 5  | FOREIGN  | Fundusz Zagraniczny 3 | 2500   | 25         |
      | 6  | MONETARY | Fundusz Pieniężny 1   | 500    | 5          |
    And not allocated amount is 1

  Scenario: Investment funds cannot be spread evenly
    Given the following investment funds:
      | id | type     | name                  |
      | 1  | POLISH   | Fundusz Polski 1      |
      | 2  | POLISH   | Fundusz Polski 2      |
      | 3  | POLISH   | Fundusz Polski 3      |
      | 4  | FOREIGN  | Fundusz Zagraniczny 1 |
      | 5  | FOREIGN  | Fundusz Zagraniczny 2 |
      | 6  | MONETARY | Fundusz Pieniężny 1   |
    When we allocate funds in amount of 10000 using SAFE strategy
    Then the allocation result is:
      | id | type     | name                  | amount | percentage |
      | 1  | POLISH   | Fundusz Polski 1      | 668    | 6.68       |
      | 2  | POLISH   | Fundusz Polski 2      | 666    | 6.66       |
      | 3  | POLISH   | Fundusz Polski 3      | 666    | 6.66       |
      | 4  | FOREIGN  | Fundusz Zagraniczny 1 | 3750   | 37.5       |
      | 5  | FOREIGN  | Fundusz Zagraniczny 2 | 3750   | 37.5       |
      | 6  | MONETARY | Fundusz Pieniężny 1   | 500    | 5          |
    And not allocated amount is 0