package pl.blueservices.mbukowicz.investmentfunds.cucumber;

import pl.blueservices.mbukowicz.investmentfunds.domain.FundType;

import java.math.BigDecimal;
import lombok.Value;

@Value
public class CukesFundAllocation {

    Long id;
    String name;
    FundType type;
    long amount;
    BigDecimal percentage;
}
