package pl.blueservices.mbukowicz.investmentfunds.cucumber;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pl.blueservices.mbukowicz.investmentfunds.allocator.FundAllocation;
import pl.blueservices.mbukowicz.investmentfunds.allocator.FundsAllocationResult;
import pl.blueservices.mbukowicz.investmentfunds.allocator.FundsAllocator;
import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;
import pl.blueservices.mbukowicz.investmentfunds.domain.InvestmentStrategy;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;

public class FundsAllocationSteps {

    private List<Fund> funds = Collections.emptyList();
    private FundsAllocator allocator = new FundsAllocator();
    private FundsAllocationResult allocationResult;

    @Given("^the following investment funds:$")
    public void the_investment_funds(DataTable fundsTable) throws Throwable {
        funds = fundsTable.asList(Fund.class);
    }

    @When("^we allocate funds in amount of (\\d+) using (\\w+) strategy$")
    public void we_allocate_funds(long amount, InvestmentStrategy strategy) throws Throwable {
        allocationResult = allocator.allocateFunds(funds, amount, strategy);
    }

    @Then("^the allocation result is:$")
    public void the_allocation_result_is(DataTable allocationsTable) throws Throwable {
        assertThat("No allocation result", allocationResult, is(notNullValue()));
        List<CukesFundAllocation> expectedAllocations = allocationsTable.asList(CukesFundAllocation.class);
        List<FundAllocation> actualAllocations = allocationResult.getAllocations();
        assertThat("Number of actual allocations <" + actualAllocations.size() +
                        "> is different than expected <" + expectedAllocations.size() + ">",
                actualAllocations, hasSize(expectedAllocations.size()));
        for (int i = 0; i < actualAllocations.size(); i++) {
            FundAllocation actual = actualAllocations.get(i);
            CukesFundAllocation expected = expectedAllocations.get(i);
            assertThat(getReason(i, "id"), actual.getFund().getId(), equalTo(expected.getId()));
            assertThat(getReason(i, "name"), actual.getFund().getName(), equalTo(expected.getName()));
            assertThat(getReason(i, "type"), actual.getFund().getType(), equalTo(expected.getType()));
            assertThat(getReason(i, "amount"), actual.getAmount(), equalTo(expected.getAmount()));
            assertTrue(getReason(i, "percentage"),
                    actual.getPercentage().compareTo(expected.getPercentage()) == 0);
        }
    }

    @Then("^not allocated amount is (\\d+)$")
    public void not_allocated_amount_is(long notAllocatedAmount) throws Throwable {
        assertThat("No allocation result", allocationResult, is(notNullValue()));
        assertThat("Not allocated amount is incorrect",
                allocationResult.getNotAllocatedAmount(), equalTo(notAllocatedAmount));
    }

    private String getReason(int i, String field) {
        return "Fund No. " + i + " has incorrect " + field;
    }
}
