package pl.blueservices.mbukowicz.investmentfunds.allocator;

import org.junit.Before;
import org.junit.Test;
import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;
import pl.blueservices.mbukowicz.investmentfunds.domain.InvestmentStrategy;

import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static pl.blueservices.mbukowicz.investmentfunds.util.FundsAllocationMatcher.hasAllocations;
import static pl.blueservices.mbukowicz.investmentfunds.util.TestFundAllocationBuilder.allocation;
import static pl.blueservices.mbukowicz.investmentfunds.util.TestFundsBuilder.funds;

public class FundsAllocatorTest {

    private FundsAllocator allocator;

    @Before
    public void setUp() {
        allocator = new FundsAllocator();
    }

    @Test
    public void shouldAllocateWholeFractionForSingleFund() {
        // given
        List<Fund> funds = funds().numPolishFunds(1).build();

        // when
        FundsAllocationResult result = allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertThat(result, hasAllocations(allocation().amount(2000).percentage(20).build()));
    }

    @Test
    public void shouldSplitFundsEvenlyAcrossTheSameFundType() {
        // given
        List<Fund> funds = funds().numPolishFunds(2).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertThat(result,
                hasAllocations(
                        allocation().amount(1000).percentage(10).build(),
                        allocation().amount(1000).percentage(10).build()));
    }

    @Test
    public void shouldAllocateFundsAcrossDifferentFundTypes() {
        // given
        List<Fund> funds = funds().numPolishFunds(2).numForeignFunds(3).numMonetaryFunds(1).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertThat(result,
                hasAllocations(
                        allocation().amount(1000).percentage(10).build(),
                        allocation().amount(1000).percentage(10).build(),
                        allocation().amount(2500).percentage(25).build(),
                        allocation().amount(2500).percentage(25).build(),
                        allocation().amount(2500).percentage(25).build(),
                        allocation().amount(500).percentage(5).build()));
    }

    @Test
    public void shouldAddRemainderAmountToTheFirstFundIfCannotSpreadEvenly() {
        // given
        List<Fund> funds = funds().numPolishFunds(3).numForeignFunds(2).numMonetaryFunds(1).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertThat(result,
                hasAllocations(
                        allocation().amount(668).percentage("6.68").build(),
                        allocation().amount(666).percentage("6.66").build(),
                        allocation().amount(666).percentage("6.66").build(),
                        allocation().amount(3750).percentage("37.5").build(),
                        allocation().amount(3750).percentage("37.5").build(),
                        allocation().amount(500).percentage(5).build()));
    }

    @Test
    public void shouldCalculateNotAllocatedAmountIfAllFundTypesAreDefined() {
        // given
        List<Fund> funds = funds().numPolishFunds(2).numForeignFunds(3).numMonetaryFunds(1).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10001, InvestmentStrategy.SAFE);

        // then
        assertEquals(1, result.getNotAllocatedAmount());
        assertEquals(10000, sumAmounts(result));
    }

    @Test
    public void shouldCalculateNotAllocatedAmountIfSingleFundTypeIsNotDefined() {
        // given
        List<Fund> funds = funds().numPolishFunds(2).numForeignFunds(3).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertEquals(500, result.getNotAllocatedAmount());
        assertEquals(9500, sumAmounts(result));
    }

    @Test
    public void shouldCalculateNotAllocatedAmountIfTwoFundTypesAreNotDefined() {
        // given
        List<Fund> funds = funds().numForeignFunds(3).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertEquals(2500, result.getNotAllocatedAmount());
        assertEquals(7500, sumAmounts(result));
    }

    @Test
    public void shouldAllocateWholeAmountIfItCanBeSpreadEvenly() {
        // given
        List<Fund> funds = funds().numPolishFunds(2).numForeignFunds(3).numMonetaryFunds(1).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 10000, InvestmentStrategy.SAFE);

        // then
        assertEquals(0, result.getNotAllocatedAmount());
        assertEquals(10000, sumAmounts(result));
    }

    @Test
    public void shouldAllocateNoAmountIfValueTooSmall() {
        // given
        List<Fund> funds = funds().numPolishFunds(2).numForeignFunds(2).numMonetaryFunds(2).build();

        // when
        FundsAllocationResult result =  allocator.allocateFunds(funds, 1, InvestmentStrategy.SAFE);

        // then
        assertEquals(1, result.getNotAllocatedAmount());
        assertEquals(0, sumAmounts(result));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfNoFundsAreDefined() {
        // given
        List<Fund> noFunds = Collections.emptyList();

        // when
        allocator.allocateFunds(noFunds, 10000, InvestmentStrategy.SAFE);

        // then exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfAmountZero() {
        // given
        List<Fund> funds = anyFunds();

        // when
        allocator.allocateFunds(funds, 0, InvestmentStrategy.SAFE);

        // then exception
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowExceptionIfAmountNegative() {
        // given
        List<Fund> funds = anyFunds();

        // when
        allocator.allocateFunds(funds, Long.MIN_VALUE, InvestmentStrategy.SAFE);

        // then exception
    }

    private long sumAmounts(FundsAllocationResult result) {
        return result.getAllocations().stream().mapToLong(FundAllocation::getAmount).sum();
    }

    private List<Fund> anyFunds() {
        return funds().numPolishFunds(3).numForeignFunds(2).numMonetaryFunds(1).build();
    }
}
