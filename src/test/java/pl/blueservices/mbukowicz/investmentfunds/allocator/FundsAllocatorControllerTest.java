package pl.blueservices.mbukowicz.investmentfunds.allocator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;
import pl.blueservices.mbukowicz.investmentfunds.domain.FundType;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(FundsAllocatorController.class)
public class FundsAllocatorControllerTest {

    public static final long FUND_ID = 1L;
    public static final String FUND_NAME = "Fundusz Polski 1";
    private static final Fund FUND = Fund.builder().id(FUND_ID).name(FUND_NAME).type(FundType.POLISH).build();
    public static final int AMOUNT = 1000;
    public static final BigDecimal PERCENTAGE = BigDecimal.TEN;
    private static final FundAllocation ALLOCATION = FundAllocation.builder()
            .fund(FUND).amount(AMOUNT).percentage(PERCENTAGE).build();
    private static final List<FundAllocation> ALLOCATIONS = Arrays.asList(ALLOCATION);
    public static final int NOT_ALLOCATED_AMOUNT = 5;
    private static final FundsAllocationResult RESULT = FundsAllocationResult.builder()
            .allocations(ALLOCATIONS).notAllocatedAmount(NOT_ALLOCATED_AMOUNT).build();

    @Autowired
    private MockMvc mvc;

    @MockBean
    private FundsAllocator allocator;

    @Test
    public void shouldReturnListOfAllocatedFunds() throws Exception {
        // given
        given(allocator.allocateFunds(anyList(), anyLong(), any())).willReturn(RESULT);
        String body = "{" +
                "\"funds\":[{\"id\": 1, \"name\": \"test fund\", \"type\": \"POLISH\"}], " +
                "\"totalAmount\": 10000, " +
                "\"strategy\": \"SAFE\"" +
                "}";

        // when
        ResultActions result = mvc.perform(
                post("/funds/allocations")
                .contentType("application/json")
                .content(body));

        // then
        result.andExpect(status().isOk())
                .andExpect(content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("allocations", hasSize(1)))
                .andExpect(jsonPath("allocations[0].fund.id").value(FUND_ID))
                .andExpect(jsonPath("allocations[0].fund.name").value(FUND_NAME))
                .andExpect(jsonPath("allocations[0].fund.type").value(FundType.POLISH.name()))
                .andExpect(jsonPath("allocations[0].amount").value(AMOUNT))
                .andExpect(jsonPath("allocations[0].percentage").value(PERCENTAGE))
                .andExpect(jsonPath("notAllocatedAmount").value(NOT_ALLOCATED_AMOUNT));
    }

}
