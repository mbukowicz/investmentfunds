package pl.blueservices.mbukowicz.investmentfunds.util;

import pl.blueservices.mbukowicz.investmentfunds.allocator.FundAllocation;

import java.math.BigDecimal;

public class TestFundAllocationBuilder {

    private final FundAllocation.FundAllocationBuilder builder = FundAllocation.builder();

    private TestFundAllocationBuilder() { }

    public static TestFundAllocationBuilder allocation() {
        return new TestFundAllocationBuilder();
    }

    public TestFundAllocationBuilder amount(long amount) {
        builder.amount(amount);
        return this;
    }

    public TestFundAllocationBuilder percentage(int percentage) {
        builder.percentage(BigDecimal.valueOf(percentage));
        return this;
    }

    public TestFundAllocationBuilder percentage(String percentage) {
        builder.percentage(new BigDecimal(percentage));
        return this;
    }

    public FundAllocation build() {
        return builder.build();
    }
}
