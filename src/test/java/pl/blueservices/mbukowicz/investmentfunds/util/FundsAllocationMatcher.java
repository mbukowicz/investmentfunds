package pl.blueservices.mbukowicz.investmentfunds.util;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import pl.blueservices.mbukowicz.investmentfunds.allocator.FundAllocation;
import pl.blueservices.mbukowicz.investmentfunds.allocator.FundsAllocationResult;

import java.util.Arrays;
import java.util.List;

public class FundsAllocationMatcher extends TypeSafeMatcher<FundsAllocationResult> {

    private final List<FundAllocation> allocations;

    public FundsAllocationMatcher(List<FundAllocation> allocations) {
        this.allocations = allocations;
    }

    @Override
    protected boolean matchesSafely(FundsAllocationResult result) {
        List<FundAllocation> allocations = result.getAllocations();
        if (allocations.size() != this.allocations.size()) {
            return false;
        }

        for (int i = 0; i < allocations.size(); i++) {
            FundAllocation expected = this.allocations.get(i);
            FundAllocation actual = allocations.get(i);

            if (expected.getAmount() != actual.getAmount() ||
                    expected.getPercentage().compareTo(actual.getPercentage()) != 0) {
                return false;
            }
        }
        return true;
    }

    @Override
    public void describeTo(Description description) {
        describeAllocations(description, allocations);
    }

    @Override
    protected void describeMismatchSafely(FundsAllocationResult result, Description description) {
        description.appendText("was ");
        describeAllocations(description, result.getAllocations());
    }

    private void describeAllocations(Description description, List<FundAllocation> allocations) {
        for (FundAllocation allocation : allocations) {
            String allocationDescription = String.format("Fund with amount %d and percentage %s\n",
                    allocation.getAmount(), allocation.getPercentage().setScale(2).toString());
            description.appendText(allocationDescription);
        }
    }

    public static FundsAllocationMatcher hasAllocations(FundAllocation... allocations) {
        return new FundsAllocationMatcher(Arrays.asList(allocations));
    }
}
