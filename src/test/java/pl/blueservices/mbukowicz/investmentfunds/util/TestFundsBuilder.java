package pl.blueservices.mbukowicz.investmentfunds.util;

import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;
import pl.blueservices.mbukowicz.investmentfunds.domain.FundType;

import java.util.ArrayList;
import java.util.List;

public class TestFundsBuilder {

    private static final String POLISH_FUND_NAME_PREFIX = "Fundusz Polski ";
    private static final String FOREIGN_FUND_NAME_PREFIX = "Fundusz Zagraniczny ";
    private static final String MONETARY_FUND_NAME_PREFIX = "Fundusz Pieniężny ";

    private int polishFundsCount = 0;
    private int foreignFundsCount = 0;
    private int monetaryFundsCount = 0;

    private TestFundsBuilder() { }

    public static TestFundsBuilder funds() {
        return new TestFundsBuilder();
    }

    public TestFundsBuilder numPolishFunds(int count) {
        this.polishFundsCount = count;
        return this;
    }

    public TestFundsBuilder numForeignFunds(int count) {
        this.foreignFundsCount = count;
        return this;
    }

    public TestFundsBuilder numMonetaryFunds(int count) {
        this.monetaryFundsCount = count;
        return this;
    }

    public List<Fund> build() {
        List<Fund> funds = new ArrayList<>();
        long id = 1;
        for (int i = 1; i <= polishFundsCount; i++) {
            funds.add(Fund.builder().id(id++).name(POLISH_FUND_NAME_PREFIX + i).type(FundType.POLISH).build());
        }
        for (int i = 1; i <= foreignFundsCount; i++) {
            funds.add(Fund.builder().id(id++).name(FOREIGN_FUND_NAME_PREFIX + i).type(FundType.FOREIGN).build());
        }
        for (int i = 1; i <= monetaryFundsCount; i++) {
            funds.add(Fund.builder().id(id++).name(MONETARY_FUND_NAME_PREFIX + i).type(FundType.MONETARY).build());
        }
        return funds;
    }
}
