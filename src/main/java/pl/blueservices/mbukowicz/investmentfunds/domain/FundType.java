package pl.blueservices.mbukowicz.investmentfunds.domain;

public enum FundType {

    POLISH, FOREIGN, MONETARY
}
