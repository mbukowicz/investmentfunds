package pl.blueservices.mbukowicz.investmentfunds.domain;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Value;

@Value
@Builder
@EqualsAndHashCode(of={"id"})
public class Fund {

    Long id;
    String name;
    FundType type;
}
