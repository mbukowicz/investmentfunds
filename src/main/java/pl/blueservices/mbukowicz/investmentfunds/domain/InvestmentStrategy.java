package pl.blueservices.mbukowicz.investmentfunds.domain;

import lombok.Getter;

public enum InvestmentStrategy {

    SAFE(20, 75, 5),
    BALANCED(30, 60, 10),
    AGGRESSIVE(40, 20, 40);

    @Getter
    private final int polishFraction;

    @Getter
    private final int foreignFraction;

    @Getter
    private final int monetaryFraction;

    InvestmentStrategy(int polishFraction, int foreignFraction, int monetaryFraction) {
        this.polishFraction = polishFraction;
        this.foreignFraction = foreignFraction;
        this.monetaryFraction = monetaryFraction;
    }
}
