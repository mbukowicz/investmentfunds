package pl.blueservices.mbukowicz.investmentfunds.allocator;

import lombok.Builder;
import lombok.Value;
import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;

import java.math.BigDecimal;

@Value
@Builder
public class FundAllocation {

    Fund fund;
    long amount;
    BigDecimal percentage;
}
