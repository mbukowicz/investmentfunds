package pl.blueservices.mbukowicz.investmentfunds.allocator;

import lombok.Value;
import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;
import pl.blueservices.mbukowicz.investmentfunds.domain.InvestmentStrategy;

import java.util.List;

@Value
public class FundsAllocationRequest {
    List<Fund> funds;
    long totalAmount;
    InvestmentStrategy strategy;
}
