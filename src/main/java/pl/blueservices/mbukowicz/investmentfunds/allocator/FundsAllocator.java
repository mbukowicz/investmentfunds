package pl.blueservices.mbukowicz.investmentfunds.allocator;

import com.google.common.base.Preconditions;
import org.springframework.stereotype.Service;
import pl.blueservices.mbukowicz.investmentfunds.domain.Fund;
import pl.blueservices.mbukowicz.investmentfunds.domain.FundType;
import pl.blueservices.mbukowicz.investmentfunds.domain.InvestmentStrategy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FundsAllocator {

    public FundsAllocationResult allocateFunds(List<Fund> funds, long totalAmount, InvestmentStrategy strategy) {
        Preconditions.checkArgument(!funds.isEmpty(), "No funds provided as input");
        Preconditions.checkArgument(totalAmount > 0, "Amount cannot be lower than or equal zero");

        List<FundAllocation> allocations = new ArrayList<>();
        allocations.addAll(calculateAllocations(funds, totalAmount, strategy.getPolishFraction(), FundType.POLISH));
        allocations.addAll(calculateAllocations(funds, totalAmount, strategy.getForeignFraction(), FundType.FOREIGN));
        allocations.addAll(calculateAllocations(funds, totalAmount, strategy.getMonetaryFraction(), FundType.MONETARY));

        long notAllocatedAmount = calculateNotAllocatedAmount(allocations, totalAmount);
        return FundsAllocationResult.builder()
                .allocations(allocations)
                .notAllocatedAmount(notAllocatedAmount).build();
    }

    private List<FundAllocation> calculateAllocations(List<Fund> allFunds, long totalAmount, long fraction, FundType fundType) {
        long fundsCount = findFundsByType(allFunds, fundType).count();

        if (fundsCount == 0) {
            return Collections.emptyList();
        }

        long amountPerFundType = totalAmount * fraction / 100;
        long amountPerFund = amountPerFundType / fundsCount;
        BigDecimal percentage = calculatePercentage(totalAmount, amountPerFund);

        List<FundAllocation> allocations = findFundsByType(allFunds, fundType)
                .map(fund -> FundAllocation.builder().fund(fund).amount(amountPerFund).percentage(percentage).build())
                .collect(Collectors.toList());

        if (fundsCount > 1) {
            long remainderAmount = amountPerFundType % fundsCount;
            if (remainderAmount > 0) {
                long firstFundAmount = amountPerFund + remainderAmount;
                BigDecimal firstFundPercentage = calculatePercentage(totalAmount, firstFundAmount);
                FundAllocation firstAllocation = FundAllocation.builder()
                        .fund(allocations.get(0).getFund())
                        .amount(firstFundAmount)
                        .percentage(firstFundPercentage)
                        .build();
                allocations.set(0, firstAllocation);
            }
        }

        return allocations;
    }

    private BigDecimal calculatePercentage(long totalAmount, long fundAmount) {
        return BigDecimal.valueOf(fundAmount, 2)
                    .multiply(BigDecimal.valueOf(100))
                    .divide(BigDecimal.valueOf(totalAmount, 2), 2, BigDecimal.ROUND_UP);
    }

    private long calculateNotAllocatedAmount(List<FundAllocation> allocations, long totalAmount) {
        return allocations.stream()
                .mapToLong(FundAllocation::getAmount)
                .reduce(totalAmount, (result, amount) -> result - amount);
    }

    private Stream<Fund> findFundsByType(List<Fund> funds, FundType type) {
        return funds.stream().filter(fund -> type.equals(fund.getType()));
    }
}
