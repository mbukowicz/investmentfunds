package pl.blueservices.mbukowicz.investmentfunds.allocator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FundsAllocatorController {

    private Logger log = LoggerFactory.getLogger(FundsAllocatorController.class);

    @Autowired
    private FundsAllocator fundsAllocator;

    @PostMapping("/funds/allocations")
    public FundsAllocationResult allocateFunds(@RequestBody FundsAllocationRequest request) {
        log.debug("Requesting funds allocation with strategy {} in amount {}",
                request.getStrategy(), request.getTotalAmount());

        return fundsAllocator.allocateFunds(request.getFunds(), request.getTotalAmount(), request.getStrategy());
    }
}
