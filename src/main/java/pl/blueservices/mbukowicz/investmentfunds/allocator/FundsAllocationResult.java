package pl.blueservices.mbukowicz.investmentfunds.allocator;

import lombok.Builder;
import lombok.Value;

import java.util.List;

@Value
@Builder
public class FundsAllocationResult {

    List<FundAllocation> allocations;
    long notAllocatedAmount;
}
