package pl.blueservices.mbukowicz.investmentfunds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IvestmentFundsApplication {

    public static void main(String[] args) {
        SpringApplication.run(IvestmentFundsApplication.class, args);
    }
}
