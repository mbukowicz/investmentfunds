# Aplikacja do wyliczania kwot dla funduszy inwestycyjnych

## Założenia

Przyjąłem pewne założenia przy przydzielaniu kwot do określonych funduszy:

* jeżeli nie ma funduszu określonego typu wtedy cała część dla tego
  typu funduszy zostaje nieprzydzielona
* wszystkie kwoty przydzielone do funduszy muszą być liczbami całkowitymi
* z tego powodu zostaje reszta z dzielenia - ta reszta staje się
  nieprzydzieloną kwotą
* czasami kwoty nie mogą być równomiernie rozdzielone pomiędzy fundusze
  określonego typu - w takim przypadku pierwszy fundusz z danego typu
  otrzymuje resztę z dzielenia

## Kompilacja

Aby skompilować kod należy uruchomić:

`./gradlew build`

## Testowanie

Do wykonania testów służy poniższa komenda:

`./gradlew test`

Dodatkowo można wygenerować raport z pokrycia testami wywołując:

`./gradlew test jacocoTestReport`

## Uruchomienie aplikacji

Jest to aplikacja Spring Bootowa, więc można ją uruchomić przy pomocy
gradle:

`./gradlew bootRun`

W rezultacie zostanie uruchomiony serwer HTTP na porcie 8080.

Działanie aplikacji można przetestować wołając:

`curl -X POST
      -H "Content-Type: application/json"
      -d '{"funds":[{"id": 1, "name": "test fund", "type": "POLISH"}], "totalAmount": 10000, "strategy": "SAFE"}'
      http://localhost:8080/funds/allocations`

## Jakość kodu (PMD i CheckStyle)

Jakość kodu można sprawdzić przy pomocy komendy:

`./gradlew check`

## Testy BDD (Cucumber)

Aby uruchomić testy BDD napisane w Cucumber należy wywołać:

`./gradlew cucumber`