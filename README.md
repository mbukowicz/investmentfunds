# Investment Funds Allocation App

[For Polish version go here](/README.pl.md)

## Assumptions

I have made some assumptions about allocating money to funds:

* if there is no fund of some type then the whole fraction related
  with this fund will be unallocated
* all amounts allocated to funds must be integer (only whole numbers are allowed)
* due to the above there are remainders when splitting money across
  funds - from this remainder unallocated money comes from
* sometimes the money cannot be evenly split between funds - in that case
  the first fund will get the uneven remainder

## How To Compile

In order to compile the source code run:

`./gradlew build`

## How To Run Tests

To run tests execute:

`./gradlew test`

If you would like to additionally generate code
coverage report then run:

`./gradlew test jacocoTestReport`

## How To Run The App

This is a Spring Boot app, which means you can run it using gradle:

`./gradlew bootRun`

As a result an HTTP server will run on port 8080.

You can then test it by running:

`curl -X POST
      -H "Content-Type: application/json"
      -d '{"funds":[{"id": 1, "name": "test fund", "type": "POLISH"}], "totalAmount": 10000, "strategy": "SAFE"}'
      http://localhost:8080/funds/allocations`

## How To Run PMD And CheckStyle

In order to run code style checks run:

`./gradlew check`

## How To Run BDD Tests (Cucumber)

You can run BDD tests like that:

`./gradlew cucumber`